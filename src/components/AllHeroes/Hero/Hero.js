import React from "react";
import { Card, Button } from "antd";

const Hero = (props) => {
  return (
    <Card
      title={props.name}
      bordered={false}
      style={{ width: 300, marginBottom: "20px", marginLeft: "10px" , display: "inline-block"}}
    >
      {props.showInfo ? (
        <>
          <p>Birth year: {props.birth_year}</p>
          <p>Height: {props.height}</p>
          <p>Mass: {props.mass}</p>
          <p>Hair color: {props.hair_color}</p>
          <p>Eye color: {props.eye_color}</p>
          <p>Gender: {props.gender}</p>
        </>
      ) : (
        <Button type="primary" onClick={props.onClick}>
          See Full Info
        </Button>
      )}
    </Card>
  );
};

export default Hero;
