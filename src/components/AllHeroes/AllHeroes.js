import React from "react";
import Hero from "./Hero/Hero";

const AllHeroes = (props) => {
  return (
    <div
      className="site-card-border-less-wrapper"
    >
      {props.heroes.map((hero, i) => {
        return (
          <Hero
            key={hero.name}
            name={hero.name}
            onClick={() => props.getFullInfo(i)}
            showInfo={false}
          />
        );
      })}
    </div>
  );
};
export default AllHeroes;
