import React from "react";
import { Modal } from "antd";

const CustomModal = (props) => {
  return (
    <Modal
      title="Detailed info"
      visible={props.isModalVisible}
      onCancel={props.handleCancel}
      onOk={props.handleCancel}
    >
      {props.children}
    </Modal>
  );
};
export default CustomModal;
