import React from "react";
import { Card, Button } from "antd";

const Planet = (props) => {
  return (
    <Card
      title={props.name}
      bordered={false}
      style={{
        width: 300,
        marginBottom: "20px",
        marginLeft: "10px",
        display: "inline-block",
      }}
    >
      {props.showInfo ? (
        <>
          <p>Rotation period: {props.rotation_period}</p>
          <p>Orbital period: {props.orbital_period}</p>
          <p>Diameter: {props.diameter}</p>
          <p>Climate: {props.climate}</p>
          <p>Gravity: {props.gravity}</p>
          <p>Terrain: {props.terrain}</p>
          <p>Surface water: {props.surface_water}</p>
          <p>Population: {props.population}</p>
        </>
      ) : (
        <Button type="primary" onClick={props.onClick}>
          See Full Info
        </Button>
      )}
    </Card>
  );
};

export default Planet;
