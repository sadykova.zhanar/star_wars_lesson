import React from "react";
import Planet from "./Planet/Planet";


const AllPlanets = (props) => {
  return (
    <div
      className="site-card-border-less-wrapper"
    >
      {props.planets.map((planet, i) => {
        return (
          <Planet
            key={planet.name}
            name={planet.name}
            onClick={() => props.getFullInfo(i)}
            showInfo={false}
          />
        );
      })}
    </div>
  );
};
export default AllPlanets;
