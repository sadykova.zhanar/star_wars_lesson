import React from "react";
import { Button } from "antd";

const PaginationItem = (props) => {
  let selectedPage = false;
  if (props.pageNumber === props.currPage) selectedPage = true;
  return (
    <Button
      type="primary"
      shape="circle"
      onClick={props.onClick}
      disabled={selectedPage}
    >
      {props.pageNumber}
    </Button>
  );
};
export default PaginationItem;
