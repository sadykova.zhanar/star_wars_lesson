import React from "react";
import PaginationItem from "./PaginationItem/PaginationItem";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";

const Pagination = (props) => {
  const pageArr = [];
  let arr = [];
  let count = 0;
  for (let i = 1; i <= props.pages; i++) {
    count++;
    arr.push(i);
    if (count === props.pageRange) {
      pageArr.push(arr);
      count = 0;
      arr = [];
    }
  }

  return (
    <div>
      <span style={{ marginRight: "10px" }}>
        <PaginationItem
          pageNumber={<LeftOutlined />}
          onClick={() => props.prevPage(pageArr[props.indexOfPageRange][0])}
        />
        {props.indexOfPageRange !== 0 ? (
          <PaginationItem
            pageNumber="..."
            onClick={() => props.prevPageRange(pageArr[props.indexOfPageRange - 1][props.pageRange-1])}
          />
        ) : null}
      </span>
      {pageArr.length !== 0
        ? pageArr[props.indexOfPageRange].map((page) => {
            return (
              <PaginationItem
                key={page}
                pageNumber={page}
                currPage={props.currPage}
                onClick={() => props.changePage(page)}
              />
            );
          })
        : null}
      <span style={{ marginLeft: "10px" }}>
        {props.indexOfPageRange !== pageArr.length - 1 ? (
          <PaginationItem
            pageNumber="..."
            onClick={() => props.nextPageRange(pageArr[props.indexOfPageRange + 1][0])}
          />
        ) : null}

        <PaginationItem
          pageNumber={<RightOutlined />}
          onClick={() => props.nextPage(pageArr[props.indexOfPageRange][props.pageRange-1])}
        />
      </span>
    </div>
  );
};

export default Pagination;
