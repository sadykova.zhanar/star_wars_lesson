import React from "react";
import { Spin, Space } from "antd";
import "./Loader.css";

const Loader = () => {
  return (
    <div className="overlay">
      <Space
        size="middle"
        style={{ width: "3rem", height: "3rem" }}
        className="Loader"
      >
        <Spin size="large" />
      </Space>
    </div>
  );
};
export default Loader;
