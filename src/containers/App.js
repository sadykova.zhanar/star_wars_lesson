import React, { Component } from "react";
import "./App.css";
import { Row, Col, Layout } from "antd";
import Heroes from "./Heroes/Heroes";
import Planets from "./Planets/Planets";

const { Header, Content } = Layout;
class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <Header>
            <h1 style={{ color: "white" , fontSize:"30px"}}>Star Wars</h1>
          </Header>
          <Content>
            <Row>
              <Col span={12}>
                <Heroes />
              </Col>
              <Col span={12}>
                <Planets />
              </Col>
            </Row>
          </Content>
        </Layout>
      </div>
    );
  }
}

export default App;
