import React, { Component } from "react";
import axios from "../../axiosBase";
import AllHeroes from "../../components/AllHeroes/AllHeroes";
import Hero from "../../components/AllHeroes/Hero/Hero";
import CustomModal from "../../components/CustomModal/CustomModal";
import Loader from "../../components/Loader/Loader";
import Pagination from "../../components/Pagination/Pagination";

class Heroes extends Component {
  state = {
    heroes: [],
    hero: null,
    pages: 0,
    pageRange: 0,
    indexOfPageRange: 0,
    currPage: 0,
    loading: false,
    error: null,
    isModalVisible: false,
  };

  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get("/people/")
      .then((response) => {
        this.setState({
          heroes: response.data.results,
          loading: false,
          currPage: 1,
        });
        const pages = Math.ceil(
          (response.data.count * 10) / response.data.results.length
        );
        this.setState({
          pages,
          pageRange: Math.ceil(pages / 10),
        });
      })
      .catch((error) => {
        this.setState({ error, loading: false });
      });
  }

  componentDidUpdate(prevProps, prevState) {
    document.addEventListener("keydown", (e) => {
      e.preventDefault();
      if (e.key === "Meta" || e.key === "Control") {
        document.addEventListener("keydown", (e) => {
          e.preventDefault();
          if (prevState.currPage === this.state.currPage) {
            if (e.key === "ArrowLeft") {
              this.goToPrevPage();
            } else if (e.key === "ArrowRight") {
              this.goToNextPage();
            }
          }
        });
      }
    });
  }

  getFullInfo = (heroId) => {
    this.setState({ hero: this.state.heroes[heroId], isModalVisible: true });
  };

  changePageNumber = (page) => {
    this.setState({ loading: true, currPage: page });
    const pageNum = page % 10 === 0 ? (page % 10) + 1 : page % 10;
    axios
      .get(`/people/?page=${pageNum}`)
      .then((response) => {
        this.setState({ heroes: response.data.results, loading: false });
      })
      .catch((error) => {
        this.setState({ error, loading: false });
      });
  };

  cancelModal = () => {
    this.setState({ isModalVisible: false });
  };

  goToPrevPage = (firstPageinRange) => {
    if (this.state.currPage !== 1) {
      if (firstPageinRange === this.state.currPage) {
        this.prevPageRange();
      }
      this.changePageNumber(this.state.currPage - 1);
    }
  };

  goToNextPage = (lastPageinRange) => {
    if (this.state.currPage !== this.state.pages) {
      if (lastPageinRange === this.state.currPage) {
        this.nextPageRange();
      }
      this.changePageNumber(this.state.currPage + 1);
    }
  };

  nextPageRange = (page) => {
    this.changePageNumber(page);
    this.setState({
      indexOfPageRange: this.state.indexOfPageRange + 1,
    });
  };

  prevPageRange = (page) => {
    this.changePageNumber(page);
    this.setState({
      indexOfPageRange: this.state.indexOfPageRange - 1,
    });
  };

  render() {
    return (
      <div style={{ borderRight: "solid", height: "100%" }}>
        <h1>Heroes</h1>
        {this.state.loading ? (
          <Loader />
        ) : (
          <>
            <AllHeroes
              heroes={this.state.heroes}
              getFullInfo={this.getFullInfo}
            />

            {this.state.hero !== null ? (
              <CustomModal
                isModalVisible={this.state.isModalVisible}
                handleCancel={this.cancelModal}
              >
                <Hero
                  showInfo={true}
                  name={this.state.hero.name}
                  birth_year={this.state.hero.birth_year}
                  height={this.state.hero.height}
                  mass={this.state.hero.mass}
                  eye_color={this.state.hero.eye_color}
                  hair_color={this.state.hero.hair_color}
                  gender={this.state.hero.gender}
                />
              </CustomModal>
            ) : null}

            <Pagination
              pages={this.state.pages}
              pageRange={this.state.pageRange}
              indexOfPageRange={this.state.indexOfPageRange}
              changePage={this.changePageNumber}
              currPage={this.state.currPage}
              prevPage={this.goToPrevPage}
              nextPage={this.goToNextPage}
              nextPageRange={this.nextPageRange}
              prevPageRange={this.prevPageRange}
            />
          </>
        )}
      </div>
    );
  }
}

export default Heroes;
