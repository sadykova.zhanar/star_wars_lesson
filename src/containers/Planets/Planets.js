import React, { Component } from "react";
import axios from "../../axiosBase";
import AllPlanets from "../../components/AllPlanets/AllPlanets";
import Planet from "../../components/AllPlanets/Planet/Planet";
import CustomModal from "../../components/CustomModal/CustomModal";
import Loader from "../../components/Loader/Loader";
import Pagination from "../../components/Pagination/Pagination";

class Planets extends Component {
  state = {
    planets: [],
    planet: null,
    pages: 0,
    pageRange: 0,
    indexOfPageRange: 0,
    currPage: 0,
    loading: false,
    error: null,
    isModalVisible: false,
  };

  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get("/planets/")
      .then((response) => {
        this.setState({
          planets: response.data.results,
          loading: false,
          currPage: 1,
        });
        const pages = Math.ceil(
          (response.data.count * 10) / response.data.results.length
        );
        this.setState({
          pages,
          pageRange: Math.ceil(pages / 10),
        });
      })
      .catch((error) => {
        this.setState({ error, loading: false });
      });
  }

  componentDidUpdate(prevProps, prevState) {
    document.addEventListener("keydown", (e) => {
      if (e.key === "Meta" || e.key === "Control") {
        document.addEventListener("keydown", (e) => {
          if (prevState.currPage === this.state.currPage) {
            if (e.key === "ArrowLeft") {
              this.goToPrevPage();
            } else if (e.key === "ArrowRight") {
              this.goToNextPage();
            }
          }
        });
      }
    });
  }

  getFullInfo = (planetId) => {
    this.setState({
      planet: this.state.planets[planetId],
      isModalVisible: true,
    });
  };

  changePageNumber = (page) => {
    this.setState({ loading: true, currPage: page });
    let pageNum = page % 10;
    switch (pageNum) {
      case 7:
      case 8:
      case 9:
        pageNum = 6;
        break;
      case 0:
        pageNum = 1;
        break;
      default:
        break;
    }
    axios
      .get(`/planets/?page=${pageNum}`)
      .then((response) => {
        this.setState({ planets: response.data.results, loading: false });
      })
      .catch((error) => {
        this.setState({ error, loading: false });
      });
  };

  cancelModal = () => {
    this.setState({ isModalVisible: false });
  };

  goToPrevPage = (firstPageinRange) => {
    if (this.state.currPage !== 1) {
      if (firstPageinRange === this.state.currPage) {
        this.prevPageRange();
      }
      this.changePageNumber(this.state.currPage - 1);
    }
  };

  goToNextPage = (lastPageinRange) => {
    if (this.state.currPage !== this.state.pages) {
      if (lastPageinRange === this.state.currPage) {
        this.nextPageRange();
      }
      this.changePageNumber(this.state.currPage + 1);
    }
  };

  nextPageRange = (page) => {
    this.changePageNumber(page);
    this.setState({
      indexOfPageRange: this.state.indexOfPageRange + 1,
    });
  };

  prevPageRange = (page) => {
    this.changePageNumber(page);
    this.setState({
      indexOfPageRange: this.state.indexOfPageRange - 1,
    });
  };

  render() {
    return (
      <div style={{ height: "100%" }}>
        <h1>Planets</h1>
        {this.state.loading ? (
          <Loader />
        ) : (
          <>
            <AllPlanets
              planets={this.state.planets}
              getFullInfo={this.getFullInfo}
            />

            {this.state.planet !== null ? (
              <CustomModal
                isModalVisible={this.state.isModalVisible}
                handleCancel={this.cancelModal}
              >
                <Planet
                  showInfo={true}
                  name={this.state.planet.name}
                  rotation_period={this.state.planet.rotation_period}
                  orbital_period={this.state.planet.orbital_period}
                  diameter={this.state.planet.diameter}
                  climate={this.state.planet.climate}
                  gravity={this.state.planet.gravity}
                  terrain={this.state.planet.terrain}
                  surface_water={this.state.planet.surface_water}
                  population={this.state.planet.population}
                />
              </CustomModal>
            ) : null}

            <Pagination
              pages={this.state.pages}
              pageRange={this.state.pageRange}
              indexOfPageRange={this.state.indexOfPageRange}
              changePage={this.changePageNumber}
              currPage={this.state.currPage}
              prevPage={this.goToPrevPage}
              nextPage={this.goToNextPage}
              nextPageRange={this.nextPageRange}
              prevPageRange={this.prevPageRange}
            />
          </>
        )}
      </div>
    );
  }
}

export default Planets;
